#!/usr/bin/env python
"""Download and catalogue emails."""

# import sys
import os
import email_client
import notifier
import storage_engine
from loguru import logger
import dotenv
import constants


def main():
    """Main function."""
    dotenv.load_dotenv()
    log_type = os.environ.get(constants.LOG_TYPE, constants.FILE_STDOUT)
    if log_type.startswith("file"):
        log_file = os.environ.get(
            constants.LOG_LOCATION,
            constants.DEFAULT_LOCATION,
        )
        if log_type == constants.FILE_ONLY:
            logger.remove(0)
        logger.add(log_file)

    logger.info("Starting to collect email.")
    server = os.environ["IMAP_SERVER"]
    username = os.environ["IMAP_USER"]
    password = os.environ["IMAP_PASSWORD"]
    client = email_client.MailClient(server, username, password)

    storage_host = os.environ["STORAGE_SERVER"]
    storage_user = os.environ["STORAGE_USER"]
    storage_password = os.environ["STORAGE_PASSWORD"]
    storage_database = os.environ["STORAGE_DATABASE"]

    hook_url = os.environ["DISCORD_URL"]
    discord_notify = notifier.DiscordNotifier(hook_url)

    storage_client = storage_engine.StorageEngine(
        storage_host, storage_user, storage_password, storage_database
    )
    with client:
        with storage_client:
            message_id = 1
            for message in client.messages():
                try:
                    logger.info(f"processing message: {message_id}")
                    message_id += 1
                    storage_client.store(message)
                    discord_notify.notify(message)
                    client.delete(message)
                except Exception as e:
                    logger.error(e)
    logger.info("All messages collected and logged")


if __name__ == "__main__":
    main()
