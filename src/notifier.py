"""Notfiers provide a means of adding notifications to detections."""

import abc
import time
import discord
import message_data


class Notifier(abc.ABC):  # pylint: disable=too-few-public-methods
    """Abstracted class for notification."""

    @abc.abstractmethod
    def notify(self, msg: message_data.MessageData):
        """Notifies stuff."""


class DiscordNotifier(Notifier):  # pylint: disable=too-few-public-methods
    """Sends notificaitons to discord."""

    def __init__(self, target_url: str):
        self._target_url = target_url
        self._message_count = 0
        self._webhook = discord.Webhook.from_url(
            self._target_url, adapter=discord.RequestsWebhookAdapter()
        )

    def notify(self, msg: message_data.MessageData):
        """Notifies stuff."""
        sha_sums = list()
        sha_sum = None
        for msg_part in msg.messages():
            if msg_part.attachment_sha256 is not None:
                sha_sums.append(
                    "\n\thttps://www.virustotal.com/gui/file/"
                    + f"{msg_part.attachment_sha256}"
                )
        if sha_sums:
            sha_sum = " ".join(sha_sums)
        else:
            sha_sum = "No Attachment"

        message = (
            f"To: {msg.to_addr}\nFrom: {msg.from_addr}\nSubject: {msg.subject}\n"
            + f"Attachment SHA256: {sha_sum}"
        )
        self._message_count += 1
        if self._message_count > 40:
            time.sleep(2)
            self._message_count = 0
        self._webhook.send(message)
